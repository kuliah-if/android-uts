package com.yuanshen.uts20523134

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Spinner
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
class MainActivity : AppCompatActivity() {
    lateinit var etNama : EditText
    lateinit var etNim : EditText
    lateinit var cbGeprek : CheckBox
    lateinit var cbEsTeh : CheckBox
    lateinit var etGeprek : EditText
    lateinit var etEstTeh : EditText
    lateinit var spPembayaran : Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etNama = findViewById(R.id.etNama)
        etNim = findViewById(R.id.etNim)
        cbGeprek = findViewById(R.id.cbGeprek)
        cbEsTeh = findViewById(R.id.cbEsTeh)
        etGeprek = findViewById(R.id.etGeprek)
        etEstTeh = findViewById(R.id.etEsTeh)
        spPembayaran = findViewById(R.id.spPembayaran)

        ArrayAdapter.createFromResource(
            this,
            R.array.jenisBayar,
            android.R.layout.simple_spinner_item
        ).also { listBayar ->
            listBayar.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spPembayaran.adapter = listBayar
        }
    }

    fun fBtnOrder(view: View){
        val sekarang = LocalDateTime.now()
        val formatTanggal = DateTimeFormatter.ofPattern("dd-MM-yyyy (HH:mm)")

        DataPesanan.tanggal = sekarang.format(formatTanggal).toString()
        DataPesanan.nama = etNama.text.toString()
        DataPesanan.nim = etNim.text.toString()
        DataPesanan.daftarPesanan = RangkumPesanan()
        DataPesanan.metodePembayaran = spPembayaran.selectedItem.toString()

        val intNotaPesanan = Intent(this, NotaPesanan::class.java)
        startActivity(intNotaPesanan)
    }

    fun RangkumPesanan(): String{
        var pesanan = ""

        if (cbGeprek.isChecked) {
            pesanan += etGeprek.text.toString() + " - "
            pesanan += cbGeprek.text.toString() + "\n"
        }

        if (cbEsTeh.isChecked) {
            pesanan += etEstTeh.text.toString() + " - "
            pesanan += cbEsTeh.text.toString() + "\n"
        }

        return pesanan
    }
}